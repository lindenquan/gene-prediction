#!/usr/bin/python
import sys
from subprocess import Popen, PIPE
import argparse
from app import db, models
import app
from psutil import Process
from config import PRJ_HOME
from config import FINAL_STATUS
from app.tool import getElapsedTime
from datetime import datetime
from os.path import exists
import re

if app.app.config['WEB_UI_LOG']:
    from weblog import logging

command = sys.argv[1:]


parser = argparse.ArgumentParser()
parser.add_argument('-pfx', dest="proj_id")
args, unknown = parser.parse_known_args()
    
print command

if app.app.config['WEB_UI_LOG']:
    logging.info(command)

o,e=Popen(command,stdout=PIPE,stderr=PIPE).communicate()

if app.app.config['WEB_UI_LOG']:
    logging.info(o)
    logging.error(e)

'''
Task has been finished
'''

# add project info into database
prj_id = args.proj_id
project = db.session.query(models.Project).get(prj_id)

project.elapsed_time = getElapsedTime(project.start_time)
project.status = FINAL_STATUS

db.session.commit()


print "pipeline is done with project - " + prj_id