from os import path
import psutil


### production environment ########
WEB_UI_LOG = False
ENABLE_CACHE = False       ### ! important, set enable_cache as True in the deployment enviroment.
DEBUG = True   ### ! important
PIPELINE_HOME='../../pipeline/'

### application ###
PORT = 7000
APP_HOME = 'app' # Do not change this value
BASE_PATH = path.abspath(path.dirname(__file__))
TEMPLATE = 'templates'
### database ###
DB_DIR = path.join(BASE_PATH, APP_HOME, 'database')
DB_PATH = path.join(DB_DIR, 'app.db')
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + DB_PATH
SQLALCHEMY_MIGRATE_REPO = path.join(DB_DIR, 'db_repository')

### website ###
WTF_CSRF_ENABLED = True
SECRET_KEY = 'you-will-never-guess'
PROJECTS ='/projects'
PRJ_HOME = BASE_PATH + PROJECTS
GENE_MARK_PL=PIPELINE_HOME+'batch_genemark_pipeline.pl'

### project ###
TOTAL_STEPS = 15
FINAL_STATUS = 'complete' ## If you change this string, you also need to change code in status.html
CANCELED ='canceled' ## If you change this string, you also need to change code in status.html
PENDING ='pending'
PROCESSING = 'processing'
WEB_LOG = 'webui.log'
START_PIPELINE = BASE_PATH+'/startPipeline.py'
### others ###
DATE_FORMAT = '%Y/%m/%d %H:%M:%S'
SAMPLE_FASTA = BASE_PATH + '/sample.fasta'
FASTA_EXTENSION = ".fasta"
OUTPUT_DIR = 'output'