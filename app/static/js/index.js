$(window).load(function() {
  init();
  //calcFingerPrint();

  $("[name='sample']").bootstrapSwitch();

  $('input[name="sample"]').on('switchChange.bootstrapSwitch', function(event, state) {
    if (state) {
      //$('#loadFile-div').hide()
      $('#loadFile-div').css("visibility", "hidden");
      //$('#div-sample-gff').show()
      $.get('sample_fasta', function(data, status) {
        if (status = 'success') {
          $('#protein_seq').val(data);
          $('#protein_seq').attr("readonly", "readonly");
        }
      });
    } else {
      $('#protein_seq').val('')
      $('#protein_seq').removeAttr("readonly");
      //$('#loadFile-div').show()
      $('#loadFile-div').css("visibility", "visible");
      //$('#div-sample-gff').hide()
       }
  });

  function success() {
    $('.sample_fasta').text('Reload sample');
    $('#seq_file').val('');
    $('.sample_fasta').removeClass('disabled');
  }

  function fail() {
    $('.sample_fasta').text('Error!');
  }

  $('#seq_file').change(function(e) {
    if (e.target.value === '') {
      // do nothing
    } else {
      $("#protein_seq").val('');
    }
  });


  $(window).click(function(e) {
    if ($('#protein_seq').val() == '') {
      if (!$('#protein_seq').is(":focus")) {

      }
    }
  });


  $('#protein_seq').on('input', function(e) {
    if (e.target.value === '') {
      // Textarea has no value
      // do nothing
    } else {
      // Textarea has a value
      $("#seq_file").val('');
    }
  });

  $('#protein_seq').focus(function(e) {
    if ($(this).hasClass('place-holder')) {
      $(this).removeClass('place-holder');
      emtpyBlack($(this));
    }
  });

  $('#protein_seq').focusout(function(e) {
    if ($(this).val() == '') {
      //loadSampleFasta();
    }
  });

  $('#ev').focus(function(e) {
    if ($(this).hasClass('place-holder')) {
      $(this).removeClass('place-holder');
      emtpyBlack($(this));
    }
  });
  $('#ev').focusout(function(e) {
    if ($(this).val() == '') {
      loadDefaultEvalue();
    }
  });

  $('#proj_name').focus(function(e) {
    if ($(this).hasClass('place-holder')) {
      $(this).removeClass('place-holder');
      emtpyBlack($(this));
    }
  });
  $('#proj_name').focusout(function(e) {
    if ($(this).val() == '') {
      loadDefaultPrjName();
    }
  });


  $('button[type="submit"]').click(function(e) {
    if($("#seq_file").val()=='' && $('#protein_seq').val()==''){
        alert("There is no input")
        e.preventDefault()
    }
  });

  $('button[type="reset"]').click(function(e) {
    let sample = $('input[name="sample"]')
    sample.bootstrapSwitch('state', false)

    loadDefaultPrjName()
    loadDefaultEvalue()
      //loadSampleFasta()
    $('#protein_seq').val('')
    $("#seq_file").val('')
    $('#gff3_file').val('')
    e.preventDefault()
  })

  handleResize($('textarea'));

  $('input.disabled').parent().css("color", "#aaaaaa");
});

function handleResize(textarea) {
  var resizeInt = null;

  // the handler function
  var resizeEvent = function() {
    locateFooter();
  };

  textarea.on('mousedown', function(e) {
    resizeInt = setInterval(resizeEvent, 100);

  });
  $(window).on("mouseup", function(e) {
    if (resizeInt !== null) {
      clearInterval(resizeInt);
    }
    resizeEvent();
  });
}

function calcFingerPrint() {
  $('input[name="fingerprint"]').val(fingerprint());
}

function init() {
  //loadSampleFasta();
  loadDefaultEvalue();
  loadDefaultPrjName();
}

function loadDefaultPrjName() {
  $('#proj_name').addClass('place-holder');
  $('#proj_name').val('Gene Prediction');
}

function loadDefaultEvalue() {
  $('#ev').addClass('place-holder');
  $('#ev').val('1e-5');
}

function loadSampleFasta() {
  $.get('/sample_fasta', function(data, status) {
    if (status = 'success') {
      $('#protein_seq').addClass('place-holder');
      $('#protein_seq').val(data);
    }
  });
}

function emtpyBlack(element) {
  element.val('');
  element.css('color', 'black');
}

