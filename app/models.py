from app import db

class Project(db.Model):
    id = db.Column(db.String(64), primary_key = True)
    name = db.Column(db.String(50))
    status = db.Column(db.String(120))
    start_time = db.Column(db.String(64))
    elapsed_time = db.Column(db.String(64))
    species = db.Column(db.String(32))
    input_amount = db.Column(db.Integer)
    pid = db.Column(db.Integer)
    pid_ctime = db.Column(db.Float)
