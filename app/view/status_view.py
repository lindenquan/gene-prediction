from app import app, db, models
from app.tool import render, getElapsedTime, kill_proc_tree
from config import PRJ_HOME, TOTAL_STEPS, FINAL_STATUS,PROCESSING, CANCELED
from datetime import datetime
from flask import request
from psutil import Process, pid_exists
from shutil import rmtree
from os.path import isdir, isfile
from bs4 import BeautifulSoup
from os import listdir

@app.route('/status')
def status():
  projects = db.session.query(models.Project).all()
  syc(projects)

  for prj in projects:  
    proj_folder=PRJ_HOME + '/' + prj.id
    if not isdir(proj_folder):
        cancel(prj.id)
        delete(prj.id)
        projects.remove(prj)
    
    elif prj.status != FINAL_STATUS and prj.status!=CANCELED:
        prj.elapsed_time = getElapsedTime(prj.start_time)
    
    db.session.commit()

  return render('status.html', projects=projects, title='Job List Status')


## this function only deletes projects and records in the database
## cancel operation must be called first from user
@app.route('/delete',methods=['POST'])
def delete_prj():
    prj_id = request.form['prj_id'].strip()
    delete(prj_id)
    return 'success'

@app.route('/cancel',methods=['POST'])
def cancel_prj():
    prj_id = request.form['prj_id'].strip()
    cancel(prj_id)
    return 'success'

def delete(prj_id):
    # delete records in database
    prj = db.session.query(models.Project).get(prj_id)

    db.session.delete(prj)
    db.session.commit()
    # delete project folder
    proj_folder=PRJ_HOME + '/' + prj.id
    if isdir(proj_folder):
        rmtree (proj_folder)

def cancel(prj_id):
    prj = db.session.query(models.Project).get(prj_id)

    if prj.status != CANCELED:
        pid = prj.pid
        if pid_exists(pid):
            process = Process(pid)
            # check if the current process is the one spawn by python by
            # comparing the create time.
            if process.create_time() == prj.pid_ctime:
                kill_proc_tree(pid)
        prj.status = 'canceled'
        db.session.commit()

def syc(projects):
    db = []
    for prj in projects:
        db.append(prj.id)
    if not isdir(PRJ_HOME):
        return
    dirs = listdir(PRJ_HOME)
    for folder in dirs:
        if folder not in db:
            rmtree (PRJ_HOME + '/' + folder)
