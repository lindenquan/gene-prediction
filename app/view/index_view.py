from app import app, db, models
from flask import request
from time import time, strptime
from config import PRJ_HOME, GENE_MARK_PL, PROCESSING, START_PIPELINE, OUTPUT_DIR
from config import DATE_FORMAT, SAMPLE_FASTA, BASE_PATH, FASTA_EXTENSION
from os import path, makedirs, chdir
from subprocess import Popen, PIPE
from datetime import datetime
from app.tool import render, countInputAmount
import requests
import re
from psutil import Process
import os 
from random import randint
from shutil import copyfile

if app.config.get('WEB_UI_LOG', None):
    from weblog import logging



@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        if request.form:
 	        return processForm()
             
    return render('index.html')

@app.route('/latestVersion')
def get_data():
    try:
        content = requests.get('https://www.ebi.ac.uk/interpro/interproscan.html').content
        m = re.search('>InterProScan (.*)<',content)
        latest = None
        now = None
        if m:
            latest = m.group(1)
        else:
            return '-1'

        proc = Popen('interproscan.sh',stdout=PIPE)
        out = proc.stdout.readline()
        Process(proc.pid).terminate()
        m = re.search('InterProScan-(.*)',out)
        if m:
            now = m.group(1)
            if now == latest:
                return '0'
            else:
                return '1'
        else:
            return '-1'
    except:
        print 'connection error. There might be no Internet connection.'
        return '-1'
    

def processForm():
    # gent project user input name
    proj_name=request.form.get('proj_name')
    fingerprint = request.form.get('fingerprint')

    if not proj_name:
    	proj_name = ""
    	  
    # generate a project
    # add finger print to avoid confliction between two projects submitted at the same time
    proj_id =fingerprint[-2:] + str(int(time()))

    # generate a project folder
    proj_folder=PRJ_HOME + '/' + proj_id
    if not path.exists(proj_folder):
    	makedirs(proj_folder)
    
    # check if there are sequences in the text area or a loaded file
    # get protein seq file name if loaded
    protein_seq_file = path.join(proj_folder, proj_id + FASTA_EXTENSION)
    
    protein_seq_str = request.form.get('protein_seq')

    if protein_seq_str:
        with open(protein_seq_file, "w") as fh:
            fh.write(protein_seq_str)
    else:
        seq_f = request.files['seq_file']
        if seq_f:
            seq_f.save(protein_seq_file)

    # get species
    species=request.form.get('species')
   
    # chnge the current folder to the newly generated project folder to run the pipeline
    chdir(proj_folder)   
    
    start_time = datetime.fromtimestamp(time()).strftime(DATE_FORMAT)

    project = models.Project(id=proj_id, status=PROCESSING, start_time = start_time, \
                            name = proj_name, species = species, input_amount = countInputAmount(protein_seq_file))

    db.session.add(project)
    db.session.flush()

    command = [START_PIPELINE, GENE_MARK_PL,
               '-d', '.',
               '-o', OUTPUT_DIR,
               '-s', species,
               '-pfx', proj_id]

    print command
    
    # Execute the pipeline program
    proc = Popen(command)
    psProcess = Process(proc.pid)

    project = db.session.query(models.Project).get(proj_id)
    project.pid = proc.pid
    project.pid_ctime= psProcess.create_time()

    db.session.commit()

    status=[proj_id, proj_name, start_time]
    lenlist=range(len(status))
    
    with app.app_context():
      pass
    return render('info.html', status=status, lenlist=lenlist, title='Info')