from app import app,db,models
from app.tool import render, zipdir
from config import PRJ_HOME,FINAL_STATUS, FASTA_EXTENSION, OUTPUT_DIR
from flask import send_from_directory, jsonify, redirect, url_for
from os.path import isfile, isdir, join
from zipfile import ZipFile, ZIP_DEFLATED
import os


class File:
    href=''
    name=''

    def __init__(self, href, name):
        self.href = href
        self.name = name   
  
@app.route('/prj_info/<prj_id>')
def prj_info(prj_id):
    gff3 = 'No'
    createZip(prj_id)
    project = db.session.query(models.Project).get(prj_id)
    
    dir = join(prj_id,OUTPUT_DIR,prj_id+FASTA_EXTENSION)
    fileList =[]
    for root, dirs, files in os.walk(PRJ_HOME+'/'+dir):
        for fileName in sorted(files):
            fileList.append(File(url_for('index')+dir+'/'+fileName, fileName))

    return render('prj_info.html', prj_name=project.name, \
                  result_zip= url_for('index') + join(prj_id, OUTPUT_DIR,prj_id +'.zip'),\
                  title='Results and Summaries',prj_id=prj_id, files = fileList)
  
@app.route('/<id>/'+OUTPUT_DIR+'/<file>.zip')
def download_prj (id,file):
  directory = join (PRJ_HOME, id,OUTPUT_DIR)
  return send_from_directory(directory= directory, filename=file+'.zip')

@app.route('/<id>/'+OUTPUT_DIR+'/<dir>/<file>')
def download_file(id,dir,file):
  directory = join (PRJ_HOME, id,OUTPUT_DIR,dir)
  return send_from_directory(directory= directory, filename=file)

def addZip(file, zipf):
    if isfile(file):
        zipf.write(file)

def createZip(id):
  directory = join(PRJ_HOME, id, OUTPUT_DIR) 
  zip_file = id +'.zip'
  if not isfile(zip_file):
    if isdir(directory):
      os.chdir(directory)
      with ZipFile(zip_file,'w', ZIP_DEFLATED) as zipf:
        if isdir(id+FASTA_EXTENSION):
          zipdir(id+FASTA_EXTENSION,zipf)


