from flask import render_template, url_for
import re
import os
from os import makedirs
from time import strftime,localtime
from psutil import Process,wait_procs,pid_exists,cpu_count,cpu_percent
from os.path import isfile, isdir
from app import app, db, models
import zipfile
from config import BASE_PATH, PRJ_HOME, DATE_FORMAT
from shutil import copy
from datetime import datetime


def render(temp, **context):
    temp_name = temp.split('.')[0]
    js = url_for('static', filename='js/'+temp_name+'.js') 
    #if isfile(app.config['PROGRAM_HOME']+js):
    context['js_file'] = js
    css = url_for('static', filename='style/'+temp_name+'.css')
    #if isfile(app.config['PROGRAM_HOME']+css):
    context['css_file'] = css
    return render_template(temp, **context)

def getElapsedTime(startTime):
    end_time = datetime.now()
    start_time = datetime.strptime(startTime, DATE_FORMAT)
    delta = end_time - start_time
    list_time = str(delta).split(':')
    list_time[2] = str(int(round(float(list_time[2]))))
    return ':'.join(list_time)


def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            subfile = os.path.join(root, file)
            ziph.write(subfile)



def countInputAmount(path):
    amount = 0
    with open(path, 'r') as f:
        for line in f:
            if re.match('^>.*',line):
                amount += 1
    return amount


def kill_proc_tree(pid, including_parent=True):
    parent = Process(pid)
    children = parent.children(recursive=True)
    for child in children:
        child.terminate()
    wait_procs(children)
    if including_parent:
        parent.terminate()