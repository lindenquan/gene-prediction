#!/usr/bin/perl -w
use strict;
# this script is to perfrom batch gene prediction for multiple files using wheat or other model model
use File::Basename;
use Cwd qw(abs_path);

use Getopt::Std;
use vars qw ($opt_d $opt_o  $opt_s);
use Time::Piece;
getopts ('d:o:s:');

my $seq_dir    = $opt_d;
my $out_dir    = $opt_o;
my $species    = $opt_s;

my $dirname = abs_path(dirname(__FILE__));

my $genemark_folder = $dirname . '/genemark_hmm_euk.linux_64';
my %species_hash = ();

$species_hash{'Wheat'} = "$genemark_folder/wheat.mtx";
$species_hash{'A. ambiae'} = "$genemark_folder/a_gambiae.mod";
$species_hash{'A. thaliana'} = "$genemark_folder/a_thaliana.mod";
$species_hash{'Barley'} = "$genemark_folder/barley.mtx";
$species_hash{'C. briggsae'} = "$genemark_folder/c_briggsae.mod";
$species_hash{'C. elegans'} = "$genemark_folder/c_elegans.mod";
$species_hash{'C. intestinalis'} = "$genemark_folder/c_intestinalis.mod";
$species_hash{'Maize'} = "$genemark_folder/corn.mtx";
$species_hash{'C. reinhardtii'} = "$genemark_folder/c_reinhardtii.mod";
$species_hash{'C. remanei'} = "$genemark_folder/c_remanei.mod";
$species_hash{'D. melanogaster'} = "$genemark_folder/d_melanogaster.mod";
$species_hash{'Human'} = "$genemark_folder/human_49_99.mtx";
$species_hash{'M. truncatula'} = "$genemark_folder/m_truncatula.mod";
$species_hash{'Rice'} = "$genemark_folder/o_sativa.mod";

my $genemark_path = $dirname . "/genemark_hmm_euk.linux_64/gmhmme2";
my $model_file = $species_hash{$species};

#my $model_file = "/opt/bio/genemark_hmm_euk.linux_64/a_thaliana.mod";

if (!$seq_dir || !$out_dir || !$species) {
    print "Usage: \n";
    print "perl batch_genemark.pl \n";
    print "     -d  a directory with fasta files \n";
	print "     -o  an output directory \n";
	print "     -s  species \n";
    exit;
}

if (not -e $out_dir) {
	print "The $out_dir does not exist. The $out_dir is created.\n";
	mkdir $out_dir;
}

my $count = 0;
opendir (DIR, $seq_dir)or die ("Can not open the directory $seq_dir: $!\n");
foreach(readdir(DIR)) {
	next if ($_ !~ /\.fa$/ && $_ !~ /\.fas$/ && $_ !~ /\.fna$/ && $_ !~ /\.fasta$/);

	$count++;
	# parsing fasta file which contains one ore more sequences
	my $seq_file = "$seq_dir/$_";
	my $seq_hash = &read_seqs($seq_file);

	my $out_dir2 = "$out_dir/$_";
	if (not -e $out_dir2) {
		mkdir $out_dir2;
	}
	
	print "[" . localtime->strftime('%Y-%m-%d %H:%M:%S') . "] $count: Gene prediction for the file $seq_file...\n";

	
	foreach my $seq_id (sort(keys(%$seq_hash))) {
		my $output_file = "$out_dir2/$seq_id" . ".genemark";
		
		my $seq_file2 = "$out_dir2/$seq_id" . ".fas";	
		open (OUT, ">$seq_file2") or die ("Can not open the file $seq_file2: $!\n");
		print OUT ">$seq_id\n";
        print OUT &format_seq($seq_hash->{$seq_id}, 70);
		close OUT;
		
		my $cmd = "$genemark_path -m $model_file -o $output_file $seq_file2";
		system $cmd;
	}
}
close DIR;


# parse results
$count = 0;
opendir (DIR, $out_dir) or dir ("Can not open the dir $out_dir $!");
while( my $subdir = readdir(DIR)){
    $subdir = "$out_dir/$subdir";
    
    next if (not -d "$subdir");
    next if ($subdir =~ /^\\.$/);
    next if ($subdir =~ /^\\.\\.$/);
        
    opendir (SUBDIR, $subdir) or dir ("Can not open the dir $subdir $!");
    while( my $filename = readdir(SUBDIR)){
        next if $filename =~ /^\.$/;
        next if $filename =~ /^\.\.$/;
        next if -d $filename;
        my $file = "$subdir/$filename";
        next if ($file !~ /\.genemark$/);
        $count++;
    
        print "$count: $file\n";
        &parse_result_file($subdir, $file);
    }
}

print "\nA total of $count sequences are annotated\n";

################################# End of the main program #########################################

################################# Subroutines #####################################################


sub read_seqs {
    my $seq_file = shift;
    open (SEQ, "<$seq_file") or die ("Can not open the file $seq_file: $!\n");
    
	my %seq_hash = ();
	
    my $found = 0;
    my $name = '';
    my %seq_sizes = ();
    my $seq = '';
    my $pre_name = '';
    my %seqs = ();
    while (my $line = <SEQ>) {
		chomp($line);
		$line = &trim($line);
		next if ($line =~ /^\s*$/);
	
		if ($line =~ /^>(\S+)/) {
		    $name = $1;
		    if ($seq ne '' && $name ne $pre_name && $pre_name ne '') {
                $seq_hash{$pre_name} = $seq;
				$seq = '';
		    }

		    $pre_name = $name;
		} else {
			$seq .= &trim($line);
		}
    }
    if ($seq ne '') {
		$seq_hash{$pre_name} = $seq;
		$seq = '';
    }
    close SEQ;
	return \%seq_hash;   
}
# Trim function to remove leading and trailing whitespaces
sub trim {
    my $string = shift;
    if ($string =~ /^\s+/) {
	$string =~ s/^\s+//;
    }
    if ($string =~ /\s+$/) {
	$string =~ s/\s+$//;
    }
    return $string;
}

sub format_seq {
    my ($seq, $line_width) = @_;
    my $len = length($seq);
    my $rows = int($len/$line_width);
    $rows++ if ($len % $line_width != 0);
    
    my $seqs = "";
    for (my $i = 0; $i < $rows; $i++) {
        my $offset = $i * $line_width;
        my $width = $line_width;
        $width = ($len - $offset) if ($i == $rows-1); 
        $seqs .= uc(substr($seq, $offset, $line_width)) . "\n";
    }
    
    return $seqs;
}


sub parse_result_file {
    my ($dir,$file) = @_;
 #   print "file = $file\n";
    my @tmps = split(/\//, $file);
    my $file_root_name = $tmps[@tmps-1];
    $file_root_name =~ s/\.genemark$//g; 
    
    my $gff3 = "$dir/$file_root_name" . ".gff3";
    my $aa_fasta = "$dir/$file_root_name"  . "_aa.fas";
    my $cds_fasta = "$dir/$file_root_name"  . "_cds.fas";
    my $genomic_fasta = "$dir/$file_root_name"  . "_genomic.fas";
    
    #my $seq_file = "$seq_dir/$file_root_name" . ".fasta.masked";
    my $seq_file = "$dir/$file_root_name" . ".fas";
    
    if (! -e $seq_file){
        print "Can not find $seq_file\n";
        exit(1);
    }
    
    my $ref_seq = &get_sequence($seq_file);
    
    open (RES, $file) or die ("Can not open the file $file $!");
    open (GFF, ">$gff3") or die ("Can not open the file $gff3 $!");
    open (FAS, ">$aa_fasta") or die ("Can not open the file $aa_fasta $!");
    open (CDS, ">$cds_fasta") or die ("Can not open the file $cds_fasta $!");
    open (GEN, ">$genomic_fasta") or die ("Can not open the file $genomic_fasta $!");
    
    my $seq_name = '';
    my $seq_len = 0;
    my $id = '';
    my $pre_id = '';
    
    my $pre_id2 = '';
    my $pre_direct = '';
    
    my $id2 = '';
    my $seq = '';
    my $gene_start = 10000000000000;
    my $gene_end = 0;
    my @cols = ();
    my $cds_seq = '';
    
    while (my $line = <RES>) {
        next if ($line =~ /^\s*$/);
        
        chomp($line);
        $line =~ s/^\s+//;  # remove leading spaces
        $line =~ s/\s+$//;  # remove endiong spaces
       
        @cols = split(/\s+/, $line);
        if ($line =~ /^Sequence name:\s+(\S+)/) {
            #$seq_name = $1;
            #my @tmps = split(/\//, $seq_name);
            $seq_name = $file_root_name;
            
  #          print "seq name = $seq_name\n";
        }
        
        elsif ($line =~ /^Sequence length:\s+(\d+)\s+bp/) {
            $seq_len = $1;
            print GFF "$seq_name\tUnknown\tSeq\t1\t$seq_len\t.\t+\t.\tID=$seq_name\n";
        }
        
        elsif (scalar(@cols) == 9  && $cols[0] =~ /^\d+$/) {
             $id = $cols[0];
            
            if ($id ne $pre_id && $pre_id ne '') {
                my $name = $seq_name . "_" . $pre_id;
                print GFF "$seq_name\tGeneMark\tgene\t$gene_start\t$gene_end\t.\t$pre_direct\t.\tID=$name\n";
                print CDS ">$name\n";
                if ($pre_direct eq '+') {
                    print CDS &format_seq($cds_seq, 70);
                } else {
                    print CDS &format_seq(&complement($cds_seq), 70);
                }
                
                print GEN ">$name\n";
                if ($pre_direct eq '+') {
                    print GEN &format_seq(&getCDS($ref_seq, $gene_start, $gene_end), 70);
                } else {
                    print GEN &format_seq(&complement(&getCDS($ref_seq, $gene_start, $gene_end)), 70);
                }

                $gene_start = $cols[4];
                $gene_end = $cols[5];
                $cds_seq = '';
            }
            my $name = $seq_name . "_" . $id;
            $gene_start = &min($gene_start, $cols[4]);
            $gene_end = &max($gene_end, $cols[5]);
            print GFF "$seq_name\tGeneMark\texon\t$cols[4]\t$cols[5]\t.\t$cols[2]\t.\tID=exon_" . $cols[1] . "_of_$name;Parent=$name\n";
    
            $cds_seq .= getCDS($ref_seq, $cols[4], $cols[5]);
                
            $pre_id = $id;
            $pre_direct = $cols[2];
        }

        elsif ($line =~ /^Predicted gene sequence(s):/) {
            # the last row of coordinates
            my $name = $seq_name . "_" . $id;
            print GFF "$seq_name\tGeneMark\tgene\t$gene_start\t$gene_end\t.\t$pre_direct\t.\tID=$name\n";
            
            #start protein sequences output
            $pre_id = '';
            $id = '';
            $seq = '';
            
        }
        elsif ($line =~ /^>(.*)$/) {   # protein sequence
            my $header = $1;
            my @tmps = split(/\|/, $header);
            $id = $tmps[2];
            ($id) = $id =~ /\S+\s+(\d+)$/;
            $id2= $tmps[1] . "|" . $tmps[3];
            if ($id ne $pre_id && $pre_id ne '') {
                if ($seq ne 'Predicted gene sequence)s):') {
                    if (uc($seq) =~ /^PREDICTED GENE SEQUENCE\(S\)\:/) {
                        $seq = substr($seq, 27);
                    }
                    if (length($seq) > 0) {
                        print FAS ">$seq_name" . "_" . $pre_id . " $pre_id2\n";
                        print FAS  &format_seq($seq, 70) . "\n";
                    }
                }
                $seq = '';
            }
            $pre_id = $id;
            $pre_id2 = $id2;
            
        } elsif (scalar(@cols) != 9 && $pre_id ne '' && $line !~ /^Predicted gene sequence(s):/) { # order is important
            $seq .= $line;
        }        
    }
  
    # print the last sequence
    my $name = "$seq_name" . "_" . $pre_id ;
    if (uc($seq) =~ /^PREDICTED GENE SEQUENCE\(S\)\:/) {
        $seq = substr($seq, 27);
    }
    if (length($seq) > 0) {
        print FAS ">$name" . " $pre_id2\n";
        print FAS  &format_seq($seq, 70) . "\n";
    }

    print CDS ">$name\n";
    print CDS  &format_seq($cds_seq, 70). "\n";
    
    print GEN ">$name\n";
    print GEN &format_seq(getCDS($ref_seq, $gene_start, $gene_end), 70);
   
    print GFF "$seq_name\tGeneMark\tgene\t$gene_start\t$gene_end\t.\t$pre_direct\t.\tID=$name\n";
             

  
    close GFF;
    close FAS;
    close CDS;
    close GEN;
}
 
sub getCDS {
    my ($ref_seq, $gene_start, $gene_end) = @_;
    
    return substr($ref_seq, $gene_start-1, $gene_end-$gene_start+1);    
}


sub get_sequence {
    my $seq_file = shift;
    my $seq = '';
    open (SEQ, $seq_file) or die ("Can not open the file $seq_file $!");

    while (my $line = <SEQ>) {
        next if ($line =~ /^\s*$/);
        chomp($line);
      
        if ($line !~ /^>/) {
            $seq .= $line;
        }
    }
    return $seq;
}


sub min {
    my ($a, $b) = @_;
    if ($a > $b) {
        return $b;
    } else {
        return $a;
    }
}

sub max {
    my ($a, $b) = @_;
    if ($a < $b) {
        return $b;
    } else {
        return $a;
    }
}


sub format_seq {
    my ($seq, $line_width) = @_;
    my $len = length($seq);
    my $rows = int($len/$line_width);
    $rows++ if ($len % $line_width != 0);

    my $seqs = "";
    for (my $i = 0; $i < $rows; $i++) {
        my $offset = $i * $line_width;
        my $width = $line_width;
        $width = ($len - $offset) if ($i == $rows-1);
        $seqs .= uc(substr($seq, $offset, $line_width)) . "\n";
    }

    return $seqs;
}

sub complement {
    my $seq = shift;
    $seq = reverse($seq);
    $seq =~ tr/ACGTacgt/TGCAtgca/;    
    return $seq;
}